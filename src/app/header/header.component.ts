import { Component, Input, Output} from '@angular/core';
import {EventEmitter} from "@angular/common/src/facade/async";

@Component({
  selector: 'mv-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  @Input() logo: string = '';
  @Output() logoChange: EventEmitter<string> = new EventEmitter<string>();

  texto: string = 'texto';

}
