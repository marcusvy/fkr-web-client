import {Component, OnInit, OnDestroy} from '@angular/core';

import {UserService} from './user.service';

@Component({
  selector: 'mv-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
})
export class UserComponent implements OnInit, OnDestroy {

  constructor(private service: UserService) {
    this.service = service;
  }

  ngOnInit() {
    console.log(this.service.ok())
  }

  ngOnDestroy() {
    console.log(this.service.ok())
  }

}
