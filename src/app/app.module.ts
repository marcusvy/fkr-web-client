import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {HeaderComponent} from './header/header.component';

import {UserModule} from "./user/user.module";
import {UserService} from "./user/user.service";
import { MegaSlideshowComponent } from './mega-slideshow/mega-slideshow.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MegaSlideshowComponent
  ],
  imports: [
    BrowserModule,
    UserModule
  ],
  providers: [UserService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
